<?php

session_start();

if(!(isset($_SESSION["id"]))){
    header("Location: index.php?error=s");
}

?>

<!-- CODE ECRIT PAR FLORI -->
<!DOCTYPE html> 
<html>
    <head>
        <meta charset="utf-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="-1">
        <link rel="stylesheet" href="/css/quizz.css">
     <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


    </head>
    <body>
        <div class="container d-flex justify-content-center vertical-center">

        <div>
        <h1><span style="color: rgb(49, 56, 203);">Bienvenue </span></h1>

        
            <p class="lead">
                Le concept est simple: il s'agit d'un quizz qui vous permettra d'évaluer vos compétences dans 
                trois matières différentes,Numérique et Sciences de l'Informatique (NSI), 
                Physique-Chimie et les Mathématiques. La difficulté peut varier d'une question à l'autre et vos
                réponses correctes seront comptabilisées à chaque validation. <br>
                Bonne chance ! Et prouvez nous que vous pouvez réussir ! 

            </p>
          
            <form method="get" action="quizz.php">
                <input type="text" name="name" placeholder="Votre nom" class="input" required>
                <button type="submit" onclick="" class="round" required><img src="https://image.flaticon.com/icons/png/512/1549/1549612.png" height="28"></button>
            </form>
        </div>
        
        </div>
          
           

        
    </body>

 
    <script src="/js/js.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</html>
<!-- FIN DU CODE ECRIT PAR FLORI -->
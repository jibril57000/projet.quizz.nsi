<?php

session_start();

if(!(isset($_SESSION["id"]))){
    header("Location: index.php?error=s");
}

if(!(isset($_GET["name"]))){
    header("Location: name.php");

}




?>

<!DOCTYPE html> 
<html>
    <head>
        <meta charset="utf-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="-1">
        <link rel="stylesheet" href="/css/quizz.css">
     <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


    </head>
    <body>

    
    <div id="correct"></div>

        <div class="container d-flex justify-content-center vertical-center " id="a"> 
            <div id="start">
                <h1>
                    Commençons le quizz <span style="color: rgb(49, 56, 203);"> <?php echo $_GET["name"] ?> </span>
                    <button type="submit" onclick="start()" class="roundb" required><img src="https://image.flaticon.com/icons/png/512/1549/1549612.png" height="28"></button>

                </h1>
            </div>


         
            <div id="questionzone">
                <div id="question"></div>
            </div>


        </div>
    </body>

    <script>
        name = '<?php echo $_GET["name"]?>'
    </script>
    <script src="/js/js.js"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</html>
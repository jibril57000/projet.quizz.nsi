score = 0;
console.log(name)

// CODE ECRIT PAR FLORI  //*

question = {
    "question1": {
        title: "Quelle est la formule de la quantité de matière",
        "p1": " n = m/M",
        "p2": " q = Z.D",
        "p3": " c = n/v",
        "p4": " p = m/v",
        "c": "p1"
    },

    "question2": {
        title: "Comment afficher une ligne dans la console Python",
        "p1": "print(x)",
        "p2": "echo x;",
        "p3": "System.out;printLn(x)",
        "p4": "puts x;",
        "c": "p1"
    }, 

    "question3": {
        title: "Quelle est la solution de l'équation 2x+2=0",
        "p1": "x=8",
        "p2": "x=2",
        "p3": "x=-1",
        "p4": "12",
        "c": "p3"
    },
    "question4": {
        title: "Quelle est la valeur de la constante d'Avogadro",
        "p1": "6,022.10²",
        "p2": "134",
        "p3": "1",
        "p4": "6,022.10²³",
        "c": "p4"
    },

    "question5": {
        title: "Pour quel réel x la fonction exponentielle est égale à 1 :",
        "p1": "x = 1.0",
        "p2": "x = 0",
        "p3": "x = 2.71",
        "p4": "x = 12",
        "c": "p2"
    },
    "question6": {
        title: "Que vaut f(x+y), f étant la fonction exponentielle ? ",
        "p1": "f(x) * f(y)",
        "p2": "f(x) / f(y)",
        "p3": "f(x) - f(y)",
        "p4": "f(x) + f(y)",
        "c": "p1"
    },
    "question7": {
        title: "Quelle est la propriété qui définit la fonction exponentielle ? ",
        "p1": "Sa dérivée s'annule en 1.",
        "p2": "Sa dérivée est égale à elle même.",
        "p3": "Elle s'annule pour x=2.",
        "p4": "Elle est décroissante pour tout x appartenant à l'enesemble des réels.",
        "c": "p2"
    },
    "question8": {
        title: "Trouver f'(x) de f(x) = sin(sqrt(e^x + a) / 2)",
        "p1": "f'(x) = (e^x*sin(sqrt(e^x+a)/2))/(4*sqrt(e^x+a))",
        "p2": "f'(x) = (e^x*cos(sqrt(e^x+a)/2))/(4*sqrt(e^x+a))",
        "p3": "f'(x) = (e^x*cos(sqrt(e^x+a)/2))/(2*sqrt(e^x+a))",
        "p4": "f'(x) = (e^x*cos(sqrt(e^x+a)/2))/(4*sqrt(e^x**2+a))",
        "c": "p2"
    },
    "question9": {
        title: "Trouver f'(x) de f(x) = 2x",
        "p1": "f'(x) = 2",
        "p2": "f'(x) = 0",
        "p3": "f'(x) = x**2",
        "p4": "f'(x) = x",
        "c": "p1"
    },
    "question10": {
        title: "Quelle est la convexité de la fonction f(x) = x**3 sur R",
        "p1": "Convexe",
        "p2": "Concave",
        "p3": "Convexe puis concave",
        "p4": "Concave puis convexe",
        "c": "p4"
    },
    "question11": {
        title: "De quelle fonction ln(|x|) est-elle la primitive ?",
        "p1": "x**2/x'",
        "p2": "x'/x",
        "p3": "x'/x**2",
        "p4": "x/x'",
        "c": "p2"
    },
    "question12": {
        title: "Quelle est la représentation graphique d'une fonction carré :",
        "p1": "C'est une parabole.",
        "p2": "C'est une hyperbole.",
        "p3": "C'est une étoile.",
        "p4": "C'est un coeur.",
        "c": "p1"
    },
    "question13": {
        title: "Qu'est-ce qu'un quantum d'énergie ?",
        "p1": "L'énergie potentielle d'un photon : E = m*g*v**2 ",
        "p2": "L'énergie cinétique d'un photon : E = 1/2 * m*g*v**2",
        "p3": "Le paquet d'énergie que transporte un photon : E = h * v**2 ",
        "p4": "Le paquet d'énergie que transporte un photon : E = h * v",
        "c": "p4"
    },
    "question14": {
        title: "HTML est language :",
        "p1": "compilé",
        "p2": "de programmation",
        "p3": "machine",
        "p4": "de balisage",
        "c": "p4"
    },
    "question15": {
        title: "Le nombre décimal 15,75 donne en binaire : ",
        "p1": "0b1111,11",
        "p2": "0b1111,01",
        "p3": "0b111,11",
        "p4": "0b1111,01",
        "c": "p1"
    },

}

// FIN DU CODE ECRIT PAR FLORI  //*

function start(){

    slideDown("start")
    displayQuestion("question1" , score)

}

function displayQuestion(qn, sc){

    score = sc;

    $( "#question" ).empty()
    $( "#question" ).append( "<h3 id='score'>Score: "+score+"/"+Object.size(question)+"</h1>" );
    $( "#question" ).append( "<h1 id='title' style='color: rgb(49, 56, 203)'></h1>" );
    document.getElementById("title").innerHTML = question[qn].title

  
    for(let i = 1; i < 5; i++){ 
        $( "#question" ).append( "<input id='" + Object.keys(question[qn])[i]+"' type='checkbox' value='" + question[qn][Object.keys(question[qn])[i]]+"'>" );
        $( "#question" ).append( "<label for='"+"&nbsp"+question[qn][Object.keys(question[qn])[i]]+"'>"+"&nbsp"+question[qn][Object.keys(question[qn])[i]]+"</label><br>" );
    }
    $( "#question" ).append( "<button onclick='next()' class='btn btn-dark' id="+ qn+">Question suivante</button>" );


}

function next(){
    let nextQ = parseInt(event.srcElement.id.replace("question", ""), 10) +1

    $( "#correct" ).empty()
    if(document.getElementById("p1").checked == true){
        if(question["question" + (nextQ-1).toString()].c == "p1") {

            if(nextQ-1 == Object.size(question)){
                score++;
                $( "#question" ).empty()
                sendWin(name,score, 2);

                $( "#question" ).append( "<h1 id='end'>Félicitations vous avez terminé le quizz</h1>" );
                $( "#question" ).append( "<h3 id='score'>Avec un score de : "+score+"/"+Object.size(question)+"</h1>" );
            } else {
                displayQuestion("question"+nextQ, score+1)
            }

            } else{


                $( "#correct" ).append( '<div class="alert alert-danger" role="alert">'+ "La réponse correcte est: "+question["question" +(nextQ-1)][question["question" + (nextQ-1)].c]+'</div>');

                if(nextQ-1 == Object.size(question)){
                    $( "#question" ).empty()
                    sendWin(name,score, 2);

                    $( "#question" ).append( "<h1 id='end'>Félicitations vous avez terminé le quizz</h1>" );
                    $( "#question" ).append( "<h3 id='score'>Avec un score de : "+score+"/"+Object.size(question)+"</h1>" );
                } else {
                    displayQuestion("question"+nextQ, score)
                }
            }
    } else if(document.getElementById("p2").checked == true) {
        if(question["question" + (nextQ-1).toString()].c == "p2") {

            if(nextQ-1 == Object.size(question)){
                score++;
                $( "#question" ).empty()
                sendWin(name,score, 2);

                $( "#question" ).append( "<h1 id='end'>Félicitations vous avez terminé le quizz</h1>" );
                $( "#question" ).append( "<h3 id='score'>Avec un score de : "+score+"/"+Object.size(question)+"</h1>" );
            } else {
                displayQuestion("question"+nextQ, score+1)
            }

            } else{
                $( "#correct" ).append( '<div class="alert alert-danger" role="alert">'+ "La réponse correcte est: "+question["question" +(nextQ-1)][question["question" + (nextQ-1)].c]+'</div>');

                if(nextQ-1 == Object.size(question)){
                    $( "#question" ).empty()
                    $( "#question" ).append( "<h1 id='end'>Félicitations vous avez terminé le quizz</h1>" );
                    $( "#question" ).append( "<h3 id='score'>Avec un score de : "+score+"/"+Object.size(question)+"</h1>" );
                } else {
                    displayQuestion("question"+nextQ, score)
                }
            }

    }else if(document.getElementById("p3").checked == true) {
        if(question["question" + (nextQ-1).toString()].c == "p3") {

            if(nextQ-1 == Object.size(question)){
                score++;
                $( "#question" ).empty()
                sendWin(name,score, 2);

                $( "#question" ).append( "<h1 id='end'>Félicitations vous avez terminé le quizz</h1>" );
                $( "#question" ).append( "<h3 id='score'>Avec un score de : "+score+"/"+Object.size(question)+"</h1>" );
            } else {
                displayQuestion("question"+nextQ, score+1)
            }

            } else{
                $( "#correct" ).append( '<div class="alert alert-danger" role="alert">'+ "La réponse correcte est: "+question["question" +(nextQ-1)][question["question" + (nextQ-1)].c]+'</div>');

                if(nextQ-1 == Object.size(question)){
                    $( "#question" ).empty()
                    sendWin(name,score, 2);

                    $( "#question" ).append( "<h1 id='end'>Félicitations vous avez terminé le quizz</h1>" );
                    $( "#question" ).append( "<h3 id='score'>Avec un score de : "+score+"/"+Object.size(question)+"</h1>" );
                } else {
                    displayQuestion("question"+nextQ, score)
                }
            }

    }else if(document.getElementById("p4").checked == true) {
        if(question["question" + (nextQ-1).toString()].c == "p4") {

            if(nextQ-1 == Object.size(question)){
                score++;
                $( "#question" ).empty()
                sendWin(name,score, 2);

                $( "#question" ).append( "<h1 id='end'>Félicitations vous avez terminé le quizz</h1>" );
                $( "#question" ).append( "<h3 id='score'>Avec un score de : "+score+"/"+Object.size(question)+"</h1>" );
            } else {
                displayQuestion("question"+nextQ, score+1)
            }

            } else{
                $( "#correct" ).append( '<div class="alert alert-danger" role="alert">'+ "La réponse correcte est: "+question["question" +(nextQ-1)][question["question" + (nextQ-1)].c]+'</div>');

                if(nextQ-1 == Object.size(question)){
                    $( "#question" ).empty()
                    sendWin(name,score, 2);

                    $( "#question" ).append( "<h1 id='end'>Félicitations vous avez terminé le quizz</h1>" );
                    $( "#question" ).append( "<h3 id='score'>Avec un score de : "+score+"/"+Object.size(question)+"</h1>" );
                } else {
                    displayQuestion("question"+nextQ, score)
                }
            }

    }
    
    

}


function sendWin(nom,finalscore,iden){
    var xhttp = new XMLHttpRequest();

    xhttp.open("GET", "/api/api.php?id=" + iden+"&name="+nom+"&score="+finalscore, true);
    xhttp.send();

    $( "#question" ).append( " <a href='/score/scores.php'>Accéder au tableau des scores</a>" );

}


function slideDown(element){
    $('#'+element).animate({'margin-top': '300px'}, 100);
    setTimeout(function(){
        $('#'+element).remove()
    }, 120)
}


Object.size = function(obj) {
    var size = 0,
      key;
    for (key in obj) {
      if (obj.hasOwnProperty(key)) size++;
    }
    return size;
  };
<?php
   session_start();
   if((isset($_SESSION["id"]))){
       header("Location: quizz/quizz.php");
   }
   $filecontent = file_get_contents("score/score.json"); 
   ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="-1">
      <link rel="stylesheet" href="/css/css.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
         integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
   </head>
   <body scroll="no" style="overflow: hidden">

  
  


      <div class="container d-flex justify-content-center vertical-center 
      ">
         <div id="login">
            <?php
               if(isset($_GET["error"])){
                       if($_GET["error"] =="s"){
                           echo '<div class="alert alert-danger" role="alert">
                           Veuillez vous connecter avant de commencer le Quizz
                       </div>';
                   }
               
               }
               if( isset($_POST["username"]) and isset($_POST["password"])) {
                   $password = $_POST["password"];
                   $username = $_POST["username"];
                   if($password == "motdepasse" and $username == "username"){
                       session_start();
                       $_SESSION["id"] = "test";
                       header("Location: quizz/quizz.php");
                   } else {
                       echo '<div class="alert alert-danger" role="alert">
                       Identifiants incorrects
                       </div>';
                   }
               
               }  
               ?>
            <h5><span style="color: rgb(49, 56, 203);">Connectez </span>vous pour accéder au quizz <br>pour répondre aux questions</h5>
            <form method="POST">
                <input type="text" name="username" placeholder="Nom">
                <br><br>
                <input type="password" name="password" class="" placeholder="Mot de Passe" required>
             
                <button type="submit" onclick="start()" class="round" required><img src="https://image.flaticon.com/icons/png/512/1549/1549612.png" height="28"></button>
            </form>

            <br>
            <a style="color: rgb(49, 56, 203);" href="https://gitlab.com/jibril57000/projet.quizz.nsi"><img style="transform: translateY(-2px);" height="16"src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png"> Code source du projet</a>

<br>
    
        <a style="color: rgb(49, 56, 203);" href="score/scores.php"><img style="transform: translateY(-2px);" height="16"src="https://image.flaticon.com/icons/png/512/49/49824.png"> Tableau des scores</a>
           
            <br>
         
         </div>
      </div>
      </div>
      </div>
   </body>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</html>